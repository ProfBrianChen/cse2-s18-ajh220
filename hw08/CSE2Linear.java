/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////                          CSE 2 HW08 part 1
////////////                       Adam Hilal                          4/9/2018

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
  
  //find grade through binary searching
  public static int Binary(int input[], int searchVal) {
    
    //initial min and max indexes of the array
    int min = 0;
    int max = 14;
    
    //counter to count iterations
    int counter = 0;
    
    while (min<=max) {
      int mid = min + (max-min)/2;
      int midVal = input[mid];
      
      counter++;
      
      //end program when midVal equals the search val
      if (midVal == searchVal) {
        return counter;
      }
      if (midVal < searchVal) {
        min = mid+1;
      }
      if (midVal > searchVal) {
        max = mid - 1;
      }
    }
    return (-1);
  }
  
  //scramble the order through multiple swaps
  public static void Scramble (int input[]) {
    Random rand = new Random();
    for (int i=0; i<15; i++) {
      int num = rand.nextInt(15);
      
      //store first value
      int temp = input[i];
      //switch first value with random index value
      input[i] = input[num];
      //insert first value into random index
      input[num] = temp;
    }
  }
  
  //evaluate every item within the array
  public static int Linear (int input[], int searchVal) {
    int counter = 0;
    for(int i=0; i<15; i++) {
      counter++;
      if (searchVal == input[i]) {
        return counter;
      }
    }
    return -1;
  }
  
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    //create grades array
    int[] grades = new int[15];
    System.out.print("Enter 15 ascending ints for grades in CSE2: ");
    for (int i=0; i<15; i++) {
      
      //scan = new Scanner(System.in);
      int posgrade;
      
      //check that user inserted grade meets all criteria
      if (scan.hasNextInt()) {
       posgrade = scan.nextInt();
      }
      else {
        System.out.println("Non-integer entered, input a new grade for student " + (i+1));
        i--;
        scan.next();
        continue;
      }

      if (posgrade >=0 && posgrade <= 100) {
      }
      else {
        System.out.println("Grade entered outside if acceptable range(1-100), input a new grade for student " + (i+1));
        i--;
        continue;
      }
      
      //assign user input value to item in array
      if (i == 0) {
        grades[i] = posgrade;
      }
      else if (i>0 && posgrade>grades[i-1]) {
        grades[i] = posgrade;
      }
      else {
        System.out.println("Non ascending integer entered, input a new grade for student " + (i+1));
        i--;
        continue;
      }
    }
    scan = new Scanner(System.in);
    System.out.print("Enter a grade to search for: ");
    int val = scan.nextInt();
    
    //runs binary to search for value
    int result = Binary(grades, val);
    if (result == -1) {
      System.out.println("Val was not found after 4 iterations");
    }
    else {
      System.out.println("Val was found after " + result + " iterations");
    }
 
    System.out.println("Scrambled: ");
    //scramble array
    Scramble(grades);
    for(int j=0; j<15; j++) {
      System.out.print(grades[j] + " ");
    }
    System.out.println("");
    System.out.print("Enter a grade to search for: ");
    int linCheck = scan.nextInt();
    
    //runs linear search to find search value
    int result2 = Linear(grades, linCheck);
    if (result2 == -1) {
      System.out.println("The value was not found in 15 iterations");
    }
    else {
      System.out.println("The value was found in " + result2 + " iterations");
    }
  }
}