///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////                 CSE HW08 Part 2
/////////////////////              Adam Hilal                           4/10/2018

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  
  //creates a new array and returns it to the main methof
  public static int[] randomInput() {
    int array[] = new int[10];
    for (int i=0; i<10; i++) {
      Random rand = new Random();
      array[i] = rand.nextInt(10);    
    }
    return array;
  }
  
  //creates a new array with all the same values except for the 1 to be deleted
  public static int[] delete(int original[], int index) {
    int[] deleted = new int[9];
    if ( index < 0 || index > 9) {
      System.out.println("The index is not valid.");
      return original;
    }
    for (int j=0; j<index;j++) {
      deleted[j] = original[j]; 
    }
    //at index, define every value for new array with index+1 of old array
    for(int i=index; i < 9; i++) {
      deleted[i] = original[i+1];
    }
    System.out.println("Element " + index + " has been found");
    return deleted;
  }
  
  public static int[] remove(int input[], int val) {
    if ( val < 0 || val > 9) {
      System.out.println("The index is not valid.");
      return input;
    }
    
    //count number of values to be removed in array
    int counter = 0;
    for (int i=0; i<=9; i++) {
      if (input[i] == val) {
        counter++;
      }
    }
    //only copy over values to new array if they are not the value which the user wishes to remove
    int []removed = new int[10-counter];
    int k =0;
    for (int j=0; j<10; j++) {
      if (input[j] != val) {
        removed[k] = input[j];
        k++;
      }
    }
    return removed;
  }
  
  //copied code
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

        System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }

  public static String listArray(int num[]){
  String out="{";
  for(int j=0;j<num.length;j++){
    if(j>0){
      out+=", ";
    }
    out+=num[j];
  }
  out+="} ";
  return out;
  }
}
