///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////                     CSE 2 HW 04
////////////////////////////       Adam Hilal February 9, 2018
//////////////////////////  


////////////////////////            Yahtzee 1 Dice Roll


import java.util.Scanner;

public class Yahtzee {
  public static void main(String[] args) {
    
    //declare and initialize variables used to count number of 1s, 2s, etc rolled on dice
    int A = 0;
    int B = 0;
    int C = 0;
    int D = 0;
    int E = 0;
    int F = 0;
    
    System.out.println("Hello! You will be playing Yahtzee!");
    
    Scanner val = new Scanner(System.in);
    
    // User decides whether to roll random or choose
    System.out.println("For game 1, would you like to pick your dice values or randomly roll? (Type \"pick\" to choose your own, or \"random\" to randomly roll)");
    String selection = val.next();
    
    
    //User selects random
    if (selection.equals("random") || selection.equals("Random")) {
      
      //Random rolls
      int dice1 = (int)(Math.random()*6)+1;
      int dice2 = (int)(Math.random()*6)+1;
      int dice3 = (int)(Math.random()*6)+1;
      int dice4 = (int)(Math.random()*6)+1;
      int dice5 = (int)(Math.random()*6)+1;
      
      //print dice values
      System.out.println( "Dice values are: " + dice1 + " " + dice2 + " " + dice3 + " " + dice4 + " " + dice5 );
      
      
      //count number of values rolled on the dice
      //Dice 1 
      if (dice1 == 1) {
        A = A+1;
      }
      else if (dice1 == 2) {
         B=B+1;
      }
      
      else if (dice1 == 3) {
         C=C+1;
      }
      
      else if (dice1 == 4) {
         D=D+1;
      }
      
      else if (dice1 == 5) {
         E=E+1;
      }
      
      else if (dice1 == 6) {
         F=F+1;
      }
      
      //Dice 2
      if (dice2 == 1) {
        A = A+1;
      }
      else if (dice2 == 2) {
         B=B+1;
      }
      
      else if (dice2 == 3) {
         C=C+1;
      }
      
      else if (dice2 == 4) {
         D=D+1;
      }
      
      else if (dice2 == 5) {
         E=E+1;
      }
      
      else if (dice2 == 6) {
         F=F+1;
      }
      
      
      //Dice 3 
      if (dice3 == 1) {
        A = A+1;
      }
      else if (dice3 == 2) {
         B=B+1;
      }
      
      else if (dice3 == 3) {
         C=C+1;
      }
      
      else if (dice3 == 4) {
         D=D+1;
      }
      
      else if (dice3 == 5) {
         E=E+1;
      }
      
      else if (dice3 == 6) {
         F=F+1;
      }
      
      //Dice 4
      if (dice4 == 1) {
        A = A+1;
      }
      else if (dice4 == 2) {
         B=B+1;
      }
      
      else if (dice4 == 3) {
         C=C+1;
      }
      
      else if (dice4 == 4) {
         D=D+1;
      }
      
      else if (dice4 == 5) {
         E=E+1;
      }
      
      else if (dice4 == 6) {
         F=F+1;
      }
      
      //Dice 5
      if (dice5 == 1) {
        A = A+1;
      }
      else if (dice5 == 2) {
         B=B+1;
      }
      
      else if (dice5 == 3) {
         C=C+1;
      }
      
      else if (dice5 == 4) {
         D=D+1;
      }
      
      else if (dice5 == 5) {
         E=E+1;
      }
      
      else if (dice5 == 6) {
         F=F+1;
      }
      
      //print number of each value
      System.out.println("1s= " + A + " 2s= " + B + " 3s= " + C + " 4s= " + D + " 5s= " + E + " 6s= " + F);
      int Bscore = 0;
      int Tscore = 0;
      int Finscore = 0;
      
      
      //computing to scorecard
      
      //Lg Straight
      if ((A >=1 && B >=1 && C >= 1 && D >= 1 && E >= 1 ) || (B >=1 && C >=1 && D >=1 && E >=1 && F >= 1)) {
        Bscore = Bscore +40;
      }
      
      //Short Straight
      else if  ((A >=1 && B >=1 && C >= 1 && D >= 1) || (B >=1 && C >=1 && D >=1 && E >=1) || ( C >=1 && D >=1 && E >=1 && F >= 1)) {
        Bscore = Bscore +30;
      }
      
      //Chance
      int Chance = dice1 + dice2 + dice3 + dice4 + dice5;
        
      
      //Bottom
      //Yahtzee
      if (A == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(A == 3 && (B == 2 || C == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (A == 4) {
        Bscore = Bscore + A*1;
      }
      //Three of a kind
      else if ( A == 3) {
        Bscore = Bscore + A*1;
      }
              
      //top section
      Tscore = Tscore + A*1;
              
      //Bottom
      //Yahtzee
      if (B == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(B == 3 && (A == 2 || C == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (B == 4) {
        Bscore = Bscore + B*2;
      }
      //Three of a kind
      else if ( B == 3) {
        Bscore = Bscore + B*2;
      }
              
      //top section
      Tscore = Tscore + B*2;
              
      
      
      //Bottom
      //Yahtzee
      if (C == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(C == 3 && (A == 2 || B == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (C == 4) {
        Bscore = Bscore + C*3;
      }
      //Three of a kind
      else if ( C == 3) {
        Bscore = Bscore + C*3;
      }
              
      //top section
      Tscore = Tscore + C*3;
      
              
      //Bottom
      //Yahtzee
      if (D == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(D == 3 && (A == 2 || B == 2 || C == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (D == 4) {
        Bscore = Bscore + D*4;
      }
      //Three of a kind
      else if ( D == 3) {
        Bscore = Bscore + D*4;
      }
              
      //top section
      Tscore = Tscore + D*4;
       
        
      //Bottom
      //Yahtzee
      if (E == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(E == 3 && (A == 2 || B == 2 || C == 2 || D == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (E == 4) {
        Bscore = Bscore + E*5;
      }
      //Three of a kind
      else if ( E == 3) {
        Bscore = Bscore + E*5;
      }
      //top section
      Tscore = Tscore + E*5;  
      
              
              
      //Bottom
      //Yahtzee
      if (F == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(F == 3 && (A == 2 || B == 2 || C == 2 || D == 2 || E ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (F == 4) {
        Bscore = Bscore + F*6;
      }
      //Three of a kind
      else if ( F == 3) {
        Bscore = Bscore + F*6;
      }
      //top section
      Tscore = Tscore + F*6;  
      
      //apply bonus if necessary
      if ( Bscore >= 63) {
        System.out.println("Bonus added to top section score worth 25");
        Bscore = Bscore + 35;
      } 
      else {
        System.out.println("No bonus was earned since top section total score was not greater than 63");
      } 
              
      //print results          
      System.out.println("Top section score = " + Tscore);        
      System.out.println("Bottom section score = " + Bscore);
      
      Finscore = Tscore + Bscore;
      
      System.out.println("Grand Total score = " + Finscore);        
      
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// User picks dice values
    else if (selection.equals("pick") || selection.equals("Pick")) {
      System.out.print("Enter Dice 1 value: ");
      int dice1 = val.nextInt();
      System.out.print("Enter Dice 2 value: ");
      int dice2 = val.nextInt();
      System.out.print("Enter Dice 3 value: ");
      int dice3 = val.nextInt();
      System.out.print("Enter Dice 4 value: ");
      int dice4 = val.nextInt();
      System.out.print("Enter Dice 5 value: ");
      int dice5 = val.nextInt();
      
      //ensure vaild dice values entered
      if ( dice1 > 6 || dice1 < 1) {
        System.out.println("Unacceptable dice value entered");
      }
      else if ( dice2 > 6 || dice2 < 1) {
        System.out.println("Unacceptable dice value entered");
      }
      else if ( dice3 > 6 || dice3 < 1) {
        System.out.println("Unacceptable dice value entered");
      }
      else if ( dice4 > 6 || dice4 < 1) {
        System.out.println("Unacceptable dice value entered");
      }
      else if ( dice5 > 6 || dice5 < 1) {
        System.out.println("Unacceptable dice value entered");
      }
      else {
      
      //count number of values entered  
      //Dice 1
      if (dice1 == 1) {
        A = A+1;
      }
      else if (dice1 == 2) {
         B=B+1;
      }
      
      else if (dice1 == 3) {
         C=C+1;
      }
      
      else if (dice1 == 4) {
         D=D+1;
      }
      
      else if (dice1 == 5) {
         E=E+1;
      }
      
      else if (dice1 == 6) {
         F=F+1;
      }
      
      //Dice 2
      if (dice2 == 1) {
        A = A+1;
      }
      else if (dice2 == 2) {
         B=B+1;
      }
      
      else if (dice2 == 3) {
         C=C+1;
      }
      
      else if (dice2 == 4) {
         D=D+1;
      }
      
      else if (dice2 == 5) {
         E=E+1;
      }
      
      else if (dice2 == 6) {
         F=F+1;
      }
      
      
      //Dice 3 
      if (dice3 == 1) {
        A = A+1;
      }
      else if (dice3 == 2) {
         B=B+1;
      }
      
      else if (dice3 == 3) {
         C=C+1;
      }
      
      else if (dice3 == 4) {
         D=D+1;
      }
      
      else if (dice3 == 5) {
         E=E+1;
      }
      
      else if (dice3 == 6) {
         F=F+1;
      }
      
      //Dice 4
      if (dice4 == 1) {
        A = A+1;
      }
      else if (dice4 == 2) {
         B=B+1;
      }
      
      else if (dice4 == 3) {
         C=C+1;
      }
      
      else if (dice4 == 4) {
         D=D+1;
      }
      
      else if (dice4 == 5) {
         E=E+1;
      }
      
      else if (dice4 == 6) {
         F=F+1;
      }
      
      //Dice 5
      if (dice5 == 1) {
        A = A+1;
      }
      else if (dice5 == 2) {
         B=B+1;
      }
      
      else if (dice5 == 3) {
         C=C+1;
      }
      
      else if (dice5 == 4) {
         D=D+1;
      }
      
      else if (dice5 == 5) {
         E=E+1;
      }
      
      else if (dice5 == 6) {
         F=F+1;
      }
      
      //print number of each value  
      System.out.println("1s= " + A + " 2s= " + B + " 3s= " + C + " 4s= " + D + " 5s= " + E + " 6s= " + F);
      int Bscore = 0;
      int Tscore = 0;
      int Finscore = 0;
      
      
      //compute scorecard
        
      //Lg Straight
      if ((A >=1 && B >=1 && C >= 1 && D >= 1 && E >= 1 ) || (B >=1 && C >=1 && D >=1 && E >=1 && F >= 1)) {
        Bscore = Bscore +40;
      }
      
      //Short Straight
      else if  ((A >=1 && B >=1 && C >= 1 && D >= 1) || (B >=1 && C >=1 && D >=1 && E >=1) || ( C >=1 && D >=1 && E >=1 && F >= 1)) {
        Bscore = Bscore +30;
      }
      
      //Chance
      int Chance = dice1 + dice2 + dice3 + dice4 + dice5;
        
      
      //Bottom
      //Yahtzee
      if (A == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(A == 3 && (B == 2 || C == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (A == 4) {
        Bscore = Bscore + A*1;
      }
      //Three of a kind
      else if ( A == 3) {
        Bscore = Bscore + A*1;
      }
              
      //top section
      Tscore = Tscore + A*1;
              
      //Bottom
      //Yahtzee
      if (B == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(B == 3 && (A == 2 || C == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (B == 4) {
        Bscore = Bscore + B*2;
      }
      //Three of a kind
      else if ( B == 3) {
        Bscore = Bscore + B*2;
      }
              
      //top section
      Tscore = Tscore + B*2;
              
      
      
      //Bottom
      //Yahtzee
      if (C == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(C == 3 && (A == 2 || B == 2 || D == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (C == 4) {
        Bscore = Bscore + C*3;
      }
      //Three of a kind
      else if ( C == 3) {
        Bscore = Bscore + C*3;
      }
              
      //top section
      Tscore = Tscore + C*3;
      
              
      //Bottom
      //Yahtzee
      if (D == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(D == 3 && (A == 2 || B == 2 || C == 2 || E == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (D == 4) {
        Bscore = Bscore + D*4;
      }
      //Three of a kind
      else if ( D == 3) {
        Bscore = Bscore + D*4;
      }
              
      //top section
      Tscore = Tscore + D*4;
       
        
      //Bottom
      //Yahtzee
      if (E == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(E == 3 && (A == 2 || B == 2 || C == 2 || D == 2 || F ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (E == 4) {
        Bscore = Bscore + E*5;
      }
      //Three of a kind
      else if ( E == 3) {
        Bscore = Bscore + E*5;
      }
      //top section
      Tscore = Tscore + E*5;  
      
              
              
      //Bottom
      //Yahtzee
      if (F == 5) {
        Bscore = Bscore +50;
      }
      //full house
      else if(F == 3 && (A == 2 || B == 2 || C == 2 || D == 2 || E ==2)) { 
        Bscore = Bscore +25;
      }
      //Four of a kind
      else if (F == 4) {
        Bscore = Bscore + F*6;
      }
      //Three of a kind
      else if ( F == 3) {
        Bscore = Bscore + F*6;
      }
      //top section
      Tscore = Tscore + F*6;  
      
      //apply bonus if necessary
      if ( Bscore >= 63) {
        System.out.println("Bonus added to top section score worth 25");
        Bscore = Bscore + 35;
      } 
      else {
        System.out.println("No bonus was earned since top section total score was not greater than 63");
      } 
              
      //print totals          
      System.out.println("Top section score = " + Tscore);        
      System.out.println("Bottom section score = " + Bscore);
      
      Finscore = Tscore + Bscore;
      
      System.out.println("Grand Total score = " + Finscore);
      }
      
    }
    else {
      System.out.println("Unacceptable respone, please play again and enter either \"pick\" or \"random\"");
    }
  }
}
    
    
    
    
