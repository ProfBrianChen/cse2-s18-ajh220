///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


import java.util.Scanner;

public class lab10 {
  public static int[][] increasingMatrix(int row, int column, boolean format) {
    
    int myArray[][];
    int counter = 1;
    
    //row-major orientation
    if (format == true) {
      myArray = new int[column][];
      for (int i=0; i <column; i++) {
        myArray[i] = new int[row];
        
        for (int j=0; j<row; j++) {
          myArray[i][j] = counter;
          counter++;
        }
      }
    }
    
    //column-major orientation
    else if (format==false){
      myArray = new int[row][];
      for (int i=0; i <column; i++) {
        myArray[i] = new int[column];
        
        for (int j=0; j<row; j++) {
          myArray[j][i] = counter;
          counter++;
        }
      }
    }
    return myArray;
  }
  public static void printMatrix(int[][] array, boolean format) {
    for (int i=0; i<array.length; i++) {
      for (int j=0; j<array[i].length; j++) {
        System.out.print(array[i][j]);
      }
      System.out.println("");
    }    
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter table width: ");
    int row = scan.nextInt();
    
    System.out.println("Enter table height: ");
    int column = scan.nextInt();
    
    System.out.println("Enter \"true\" for row-major format or \"false\" for column-major format: ");
    Boolean format = scan.nextBoolean();
    
    int[][] myArray = increasingMatrix(row, column, format);
    printMatrix(myArray, format);
  }
}