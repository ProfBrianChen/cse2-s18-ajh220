/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  CSE2 hw02  Arithmetic
//////////////////////////////////// Adam Hilal

public class Arithmetic {
  public static void main(String[] args) {
     
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //decarling and solving totalvariables
    double totpantsprice=numPants*pantsPrice;
    double totshirtprice=numShirts*shirtPrice;
    double totbeltprice=numBelts*beltCost;
    
    //declaring and solving sales tax variables
    int pantsst=(int)(paSalesTax*totpantsprice*100);
    int shirtst=(int)(paSalesTax*totshirtprice*100);
    int beltst=(int)(paSalesTax*totbeltprice*100);
  
    //redefine integer to double
    double pantsst2=pantsst;
    double shirtst2=shirtst;
    double beltst2=beltst;
    
    //corrects sales tax to right decimal points
    pantsst2=pantsst2/100;
    shirtst2=shirtst2/100;
    beltst2=beltst2/100;    
    
    //print costs and taxes per items
    System.out.println("");
    System.out.println("The cost of "+numPants+" pairs of pants before tax is $"+totpantsprice+", and the sales tax is $"+pantsst2);
    System.out.println("The cost of "+numShirts+" shirts before tax is $"+totshirtprice+", and the sales tax is $"+shirtst2);
    System.out.println("The cost of "+numBelts+" belt before tax is $"+totbeltprice+", and the sales tax is $"+beltst2);
    
    //total cost before tax
    double totcostbt=totpantsprice+totshirtprice+totbeltprice;
    
    //total sales tax
    double totst=pantsst2+shirtst2+beltst2;
    
    //total purchase cost;
    double totalcost=totcostbt+totst;
     
    //prints costs of combined items and taxes
    System.out.println("");
    System.out.println("The total cost of all items before tax is $"+totcostbt+", and the combined sales tax is $"+totst);
    System.out.println("The total cost of all items plus tax is $"+totalcost);   

  }
}