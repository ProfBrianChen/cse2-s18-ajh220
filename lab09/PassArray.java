////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////                      CSE 2
///////////////////           Adam Hilal                  4/19/2018


public class PassArray {
  
  //copies an array in new memory
  public static int[] copy(int[] array) {
    int []newArray = new int[array.length];
    newArray = array;
    return newArray;
  }
  //inverts pointer array where change persists outside of method
  public static void inverter(int[] array) {
    for (int i=0; i<array.length/2; i++) {
      int temp = array[i];
      array[i] = array[(array.length-1)-i];
      array[(array.length-1)-i] = temp;
    }
  }
  //inverts copy array which does not affect orginal array
  public static int[] inverter2(int[] array) {
    int[] newArray = new int[array.length];
    newArray = copy(array);
    for (int i=0; i<newArray.length/2; i++) {
      int temp = newArray[i];
      newArray[i] = newArray[(newArray.length-1)-i];
      newArray[(newArray.length-1)-i] = temp;
    }
    return newArray;
  }
  
  // print array values
  public static void print(int[] array) {
    for (int i=0; i<array.length; i++) {
      System.out.print(array[i] + " ");
    }
    System.out.println("");
  }
  public static void main(String[] args) {
    int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7};
    
    inverter(array0);
    print(array0);
    
    int array1[] = inverter2(array0);
    print(array1);
    
    int array2[] = inverter2(array1);
    print(array2);
  }
}