/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////     CSE2 Lab 3
///////////////////////////////////////////     Adam Hilal 2/9/2018

import java.util.Scanner;    //define the scanner for program

public class Check{
  public static void main(String[] args)  {
    Scanner myScanner = new Scanner(System.in);      //declaring instance of an a scanner for input
    
    //accepting input of check cost
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();             
    
    //accepting percentage you wish to tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    
    //accepting input of number of people who are splitting check
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //declaring values for calculating split check
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    
    //calculating split check numbers
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars= (int) costPerPerson;
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    //displaying final value each person owes
    System.out.println("Each person in the group owes $"+dollars+"."+dimes+pennies);
    }
}