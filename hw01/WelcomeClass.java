////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// CSE 2 Welcome Class
////////////////////// Homework 1


public class WelcomeClass {
  public static void main(String[] args) {
    
    //prints the specified message
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("   ^  ^  ^  ^  ^  ^");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
    //prints my specific Lehigh ID
    System.out.println(" <-A--J--H--2--2--0->");
    //
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v");
  }
}
