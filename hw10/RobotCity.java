/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////        CSE 2 Hw10
///////////////                   Adam Hilal                  4/23/2018

// Program creates a multidimensional array of a random size and randomly assigns "robots' to certain location
// and modifies the data based on their prescence


import java.util.Random;

public class RobotCity {
  public static int[][] buildCity() {
    Random rand = new Random();
    
    //create a random length for 1st(North/South array)
    int[][] cityArray = new int[rand.nextInt(6)+10][];
    
    //create a random length for interior(East/West arrays) and assign using the for loop
    int EWlength = rand.nextInt(6)+10;
    for (int i=0; i<cityArray.length; i++) {
      cityArray[i] = new int[EWlength];
    }
    
    //assign random values to every block
    for (int j=0; j<cityArray.length; j++) {
      for (int k=0; k<cityArray[j].length; k++) {
        cityArray[j][k] = rand.nextInt(900)+100;
      }
    }
    return cityArray;
  }
  
  public static int[][] invade(int[][] cityArray, int k) {
    Random rand = new Random();
    
    //create a robot array with same dimensions as city to keep track of robots
    int[][] robotArray = new int[cityArray.length][];
    for (int l=0; l<robotArray.length; l++) {
      robotArray[l] = new int[cityArray[l].length];
    }
    
    //assign a "no robot" value of zero
    for (int m=0; m<cityArray.length; m++) {
      for (int n=0; n<cityArray[m].length; n++) {
        robotArray[m][n] = 0;
      }
    }
    
    //assign robots to open spots on grid and denote occupancy with a 1
    for (int i=1; i<=k; i++) {
      while (true) {
        int NS = rand.nextInt(robotArray.length);
        int EW = rand.nextInt(robotArray[0].length);
        if (robotArray[NS][EW] == 0) {
          robotArray[NS][EW] = 1;
          break;
        }
      }
    }
    
    //if there is a robot on a city square, display negative va;ue of population
    for (int x=0; x<cityArray.length; x++) {
      for (int y=0; y<cityArray[0].length; y++) {
        if (robotArray[x][y] == 1) {
          cityArray[x][y] -= (2*cityArray[x][y]);
        }
      }
    }
    return robotArray;
  }
  
  public static void update(int[][] cityArray, int[][] robotArray) {
    
    //update robot positioning
    for (int i=0; i< robotArray.length; i++) {
      for (int k=robotArray[0].length-1; k>0; k--) {
        
        //takes care of eastmost column
        if (k==(robotArray[0].length-1) && robotArray[i][k] == 1) {
          robotArray[i][robotArray[0].length-1] = 0;
        }
        if (robotArray[i][k-1] == 1) {
          robotArray[i][k-1] = 0;
          robotArray[i][k] = 1;
        }        
      }
    }
    
    //update city blocks with robots
    for (int m=0; m< cityArray.length; m++) {
      for (int n=0; n< cityArray[0].length; n++) {
        if (robotArray[m][n] == 0 && cityArray[m][n] < 0) {
          cityArray[m][n] += (-2*cityArray[m][n]);
        }
        if (robotArray[m][n] == 1 && cityArray[m][n] > 0) {
          cityArray[m][n] -= (2*cityArray[m][n]);
        }
      }
    }
  }
  
  //print out city blocks with array[0][] as south and array[][0] as west
  public static void display(int[][] Array) {
    for (int i=(Array.length-1); i>=0; i--) {
      for (int j=0; j<Array[0].length; j++) {
        System.out.print(" ");        
        System.out.printf("%4d", Array[i][j]);          
        System.out.print(" ");
      }
      System.out.println();
    }
    System.out.println();
    return;
  }
  
  public static void main(String[] args) {
    Random rand = new Random();
    
    int[][] cityArray = buildCity();
    int robots = rand.nextInt((cityArray.length)*(cityArray[0].length));
    
    System.out.println("North/South Array length: " + cityArray.length);    
    System.out.println("East/West Array length: " + cityArray[0].length);
    System.out.println("Number of robots: " + robots);
    System.out.println();
    
    System.out.println("Orignal Array: ");
    display(cityArray);    
    int [][] robotArray = invade(cityArray, robots);
    
    System.out.println("Invaded Array: ");
    display(cityArray);
    
    for (int i=1; i<=5; i++) {
      update(cityArray, robotArray);
      
      System.out.println("Update " + i + " Array: ");
      display(cityArray);
    }
  }
}