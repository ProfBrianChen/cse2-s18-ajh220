/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//


import java.util.Random;
import java.util.Scanner;

public class RandomStory {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    while (true) {
      scan = new Scanner(System.in);
      String adjective1 = adjectives();
      String firstnoun = nouns();
      String verb = pastverbs();
      String adjective2 = adjectives();
      String secondnoun = secondnouns();
      System.out.println("");
      System.out.println("The " + adjective1 + " " + firstnoun + " " + verb + " the " + adjective2 + " " + secondnoun);
      
      String adjective3 = adjectives();
      String actionverb = actverbs();
      String adjective4 = adjectives();
      String noun3 = newnouns();
      System.out.println("The " + adjective3 + " " + firstnoun + " " + actionverb + " " + adjective4 + " " + noun3);
      
      String finalverb = lastverbs();
      String noun4 = lastnouns();
      System.out.println("The " + firstnoun + " " + finalverb + " her " + noun4 + "!");
      System.out.println("");
      System.out.println("would you like another paragraph?(input yes or no) ");
      String val = scan.nextLine();
      if (val.equals("yes")) {
        continue;
      }
      else {
        break;
      }
    }
    
    
  }
  public static String adjectives() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String adjective = "";
    
    switch(randomint) {
      case 1:
        adjective = "cheerful";
        break;
      case 2:
        adjective = "fun";
        break;
      case 3:
        adjective = "happy";
        break;
      case 4:
        adjective = "funny";
        break;
      case 5:
        adjective = "excited";
        break;
      case 6:
        adjective = "silly";
        break;
      case 7:
        adjective= "joyous";
        break;
      case 8:
        adjective = "amusing";
        break;
      case 0:
        adjective = "smiling";
        break;
    }
    return adjective;
  }
   public static String nouns() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String noun = "";
    
    switch(randomint) {
      case 1:
        noun = "man";
        break;
      case 2:
        noun = "woman";
        break;
      case 3:
        noun = "teenager";
        break;
      case 4:
        noun = "grandpa";
        break;
      case 5:
        noun = "aunt";
        break;
      case 6:
        noun = "daughter";
        break;
      case 7:
        noun= "grandma";
        break;
      case 8:
        noun = "child";
        break;
      case 0:
        noun = "uncle";
        break;
    }
    return noun;
  }
  public static String pastverbs() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String verb = "";
    
    switch(randomint) {
      case 1:
        verb = "laughed with";
        break;
      case 2:
        verb = "smiled at";
        break;
      case 3:
        verb = "danced with";
        break;
      case 4:
        verb = "cooked for";
        break;
      case 5:
        verb = "talked with";
        break;
      case 6:
        verb = "said hi to";
        break;
      case 7:
        verb= "told a story to";
        break;
      case 8:
        verb = "watched tv with";
        break;
      case 0:
        verb = "looked at";
        break;
    }
    return verb;
  }
   public static String secondnouns() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String noun = "";
    
    switch(randomint) {
      case 1:
        noun = "man";
        break;
      case 2:
        noun = "woman";
        break;
      case 3:
        noun = "teenager";
        break;
      case 4:
        noun = "grandpa";
        break;
      case 5:
        noun = "aunt";
        break;
      case 6:
        noun = "daughter";
        break;
      case 7:
        noun= "grandma";
        break;
      case 8:
        noun = "child";
        break;
      case 0:
        noun = "uncle";
        break;
    }
    return noun;
  }
  public static String actverbs() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String verb = "";
    
    switch(randomint) {
      case 1:
        verb = "built";
        break;
      case 2:
        verb = "ate";
        break;
      case 3:
        verb = "created";
        break;
      case 4:
        verb = "cooked";
        break;
      case 5:
        verb = "hit";
        break;
      case 6:
        verb = "smacked";
        break;
      case 7:
        verb= "drank";
        break;
      case 8:
        verb = "wrote";
        break;
      case 0:
        verb = "ran";
        break;
    }
    return verb;
  }
  public static String newnouns() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String noun = "";
    
    switch(randomint) {
      case 1:
        noun = "warplanes";
        break;
      case 2:
        noun = "deer";
        break;
      case 3:
        noun = "windows";
        break;
      case 4:
        noun = "rooms";
        break;
      case 5:
        noun = "cars";
        break;
      case 6:
        noun = "rocket launchers";
        break;
      case 7:
        noun= "houses";
        break;
      case 8:
        noun = "pencils";
        break;
      case 0:
        noun = "roads";
        break;
    }
    return noun;
  }
  public static String lastverbs() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(4);
    
    String verb = "";
    
    switch(randomint) {
      case 1:
        verb = "knew";
        break;
      case 2:
        verb = "studied";
        break;
      case 3:
        verb = "learned";
        break;
    }
    return verb;
  }
  public static String lastnouns() {
    Random randomgen = new Random();
    int randomint = randomgen.nextInt(9);
    
    String noun = "";
    
    switch(randomint) {
      case 1:
        noun = "warplanes";
        break;
      case 2:
        noun = "deer";
        break;
      case 3:
        noun = "window";
        break;
      case 4:
        noun = "room";
        break;
      case 5:
        noun = "car";
        break;
      case 6:
        noun = "rocket launcher";
        break;
      case 7:
        noun= "house";
        break;
      case 8:
        noun = "pencil";
        break;
      case 0:
        noun = "road";
        break;
    }
    return noun;
  }
}