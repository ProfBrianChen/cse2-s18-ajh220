///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                     CSE 2
////////////////////////////////           Adam Hilal            March 10, 2018
//
//                                           Lab 06



import java.util.Scanner;

public class encrypted_x {
  public static void main(String[] args ) {
    
    Scanner scan = new Scanner(System.in);
    
    int input = 0;
    
    System.out.println("Enter an integer between 1 and 100");
    
    //repeat loop if unacceptable value entered
    while(true) {
      
      scan = new Scanner(System.in);
      
      //check for integer
      if(scan.hasNextInt()) {
        input = scan.nextInt();
        
        //check for integer in correct range
        if (input >= 0 && input <= 100) {
          
          //creates height of output
          for (int i=0; i <= input; i++) {
            
            //creates width of output
            for (int j=0; j <= input; j++) {
              
              //creates x pattern
              if ( j == i ) {
                System.out.print(" ");
              }
              else if ( j == (input - i)) {
                System.out.print(" ");
              }
              else {
                System.out.print("*");
              }

            }
            System.out.println(" ");
          }
          break;
        }
        else {
          System.out.println("Unaccpetable value, enter an int between 0 and 100");
        }
      }
      else {
        System.out.println("Unacceptable value, enter an int between 0 and 100");
      }
    }
  }
}