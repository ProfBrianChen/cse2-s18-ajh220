///////////////////////////////////////////////////////////////////////////
///////////////////////                             CSE2 lab08
///////////                    Adam Hilal                            4/8/2018

//create a program that creates a random sized array, accept user input for each row of array 
//and randomly create grade for each user input and display resutls

import java.util.Scanner;

public class Students {
  public static void main(String[] args) {
    
    
    Scanner scan = new Scanner(System.in);
    
    //determine random size of array
    int size = (int)(Math.random()*6+5);
    
    //create array and label as string array
    String[] students = new String[size];
    
    System.out.println("Enter " + size + " students names:");
    
    //accepte student names and input into new row of array
    for (int i=0; i<size; i++) {
      students[i] = scan.nextLine();
    }
    
    //declare and label int array for students grades
    int[] grades = new int[size];
    for(int j=0; j<size; j++) {
      //randomly create grades for each student
      grades[j] = (int)(Math.random()*100+1);
    }
    
    System.out.println("Here are the midterm grades of the " + size + " students above:");
    //print results
    for(int k=0; k<size; k++) {
      System.out.println(students[k] + ": " + grades[k]);
    }
  }
}