////////////////////////////////

// Lower right X with diamond behind

//  THIS ONE WORKS
// Creates both X with proper stripe and diamond


import java.util.Scanner;

public class PracArgyle8 {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter height int: ");
    int height = scan.nextInt();
    
    System.out.println("Enter total width int: ");
    int twidth = scan.nextInt();
    
    System.out.println("Enter Argyle diamond width: ");
    int dwidth = scan.nextInt();
    
    System.out.println ("Enter a positive odd integer for the width of the argyle center stripe: ");
    int stripe = scan.nextInt();
    
    int diamonds = (twidth/(dwidth*2));
    int hdiamonds = (height/(dwidth*2));
    
    int remdiamonds = twidth%(dwidth*2);
    int remhdiamonds = height%(dwidth*2);
    
    System.out.println(diamonds);
    System.out.println(remdiamonds);
    
    int z = 0;
    int y = 0;
    
    //BOTTOM HALF
    
    //creates height of top half of diamond
    for (int k = 1; k <= dwidth; k++) {
      //creates number of horizontal diamonds
      for ( int j =1; j <= diamonds; j ++) {
        //creates lower right half of diamond width which accounts for stripe width
        for ( int i = 1; i <= dwidth-stripe/2; i++) {
          if ( i >= (k - stripe/2) && i<k && z == 0) {
            for ( int n = 1; n <=k-i; n++){
              System.out.print("#");
            }
            z++;
          }
          else if ( i <= dwidth - k && i < k-stripe/2) {
            System.out.print("@");
          }
          else if ( i < (k-stripe/2)) {
            System.out.print("-");
          }
          
          if (i == k) {
            for (int m = 1; m <= stripe/2+1; m++) {
              System.out.print("#");
            }
          }
          else if ( i <= (dwidth-stripe/2) - k && i >= k + stripe/2 ) {
            System.out.print("@");
          }
          else if (i > k) {
            System.out.print("-");
          }
        }
        if( k > dwidth-stripe/2) {
          for( int q = k; q<= dwidth; q++) {
            System.out.print("#");
          }
        }
      }
      System.out.println(" ");
      y =0;
      z =0;
    }
  }
}