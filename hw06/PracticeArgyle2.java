//////////////////////////////

import java.util.Scanner;

public class PracticeArgyle2 {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter height int: ");
    int height = scan.nextInt();
    
    System.out.println("Enter total width int: ");
    int twidth = scan.nextInt();
    
    System.out.println("Enter Argyle diamond width: ");
    int dwidth = scan.nextInt();
    
    int diamonds = (twidth/(dwidth*2));
    int hdiamonds = (height/(dwidth*2));
    
    int remdiamonds = twidth - (diamonds*(2*dwidth));
    int remhdiamonds = height - (hdiamonds*(2*dwidth));
    
    System.out.println(diamonds);
    System.out.println(remdiamonds);
    
    //creates full vertical diamonds first
    if ( height >= (2*dwidth)) {
      //creates number of vertical diamonds 
      for (int w = 1; w <= hdiamonds; w++) {
        //creates height for 1 diamond
        for( int i = 0; i < dwidth; i++) {
          //creates number of horizontal diamonds
          for (int h =1; h <= diamonds; h++) {
            //creates half upper left half of diamond
            for ( int j = 0; j < dwidth; j++) {
              if (j == i) {
                System.out.print("#");
              }
              else if (j > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            //creates upper right half of diamond
            for (int k = 0; k < dwidth; k++) {
              if ( k == dwidth - i ) {
                System.out.print("#");
              }
              else if ( k < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          //creates remaining diamond width
          if(remdiamonds <= dwidth) {
            for ( int n = 0; n < remdiamonds; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          else if (remdiamonds > dwidth) {
            for ( int n = 0; n <= dwidth; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            for (int p = 0; p <= (remdiamonds - dwidth); p++) {
              if ( p == dwidth - i ) {
                System.out.print("#");
              }
              else if ( p < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          //for (int m = 1; m <= twidth - diamonds*dwidth) {

          //}
          System.out.println(" ");
        }

        //SECOND ROW - Bottom half of diamond 

        //creates vertical rows for bottom half of diamond
        for( int i = 0; i <= dwidth; i++) {
          //creates user specified number of diamonds 
            for ( int h =1; h <= diamonds; h++) {
              //creates lower left half of diamond 
              for ( int k = 0; k < dwidth; k++) {
                if ( k == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( k > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
              //creates lower right half of diamond
              for ( int j = 0; j < dwidth; j++) {
                if (j == i) {
                  System.out.print("#");
                }
                else if (j < (dwidth-i)){
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
          //creates remaining width for diamond
            if(remdiamonds <= dwidth) {
              for ( int n = 0; n < remdiamonds; n++) {
                if ( n == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( n > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
            else if (remdiamonds > dwidth) {
              for ( int n = 0; n <= dwidth; n++) {
                if ( n == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( n > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
              for ( int n = 0; n <= (remdiamonds-dwidth); n++) {
                if (n == i) {
                  System.out.print("#");
                }
                else if (n < (dwidth-i)){
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
            //for (int m = 1; m <= twidth - diamonds*dwidth) {

            //}
            System.out.println(" ");
          }
      }
      
      //create remaining vertical diamonds
      if (remhdiamonds <= dwidth) {
          //creates height for remaining rows
        for( int i = 0; i < remhdiamonds; i++) {
          //creates number of horizontal diamonds
          for (int h =1; h <= diamonds; h++) {
            //creates half upper left half of diamond
            for ( int j = 0; j <= dwidth; j++) {
              if (j == i) {
                System.out.print("#");
              }
              else if (j > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            //creates upper right half of diamond
            for (int k = 0; k <= dwidth; k++) {
              if ( k == dwidth - i ) {
                System.out.print("#");
              }
              else if ( k < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          //creates remaining diamond width
          if(remdiamonds <= dwidth) {
            for ( int n = 0; n < remdiamonds; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          else if (remdiamonds > dwidth) {
            for ( int n = 0; n <= dwidth; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            for (int p = 0; p <= (remdiamonds - dwidth); p++) {
              if ( p == dwidth - i ) {
                System.out.print("#");
              }
              else if ( p < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          System.out.println(" ");
        }
      }
      else if (remhdiamonds > dwidth) {
        //creates top half of diamond
        for( int i = 0; i <= dwidth; i++) {
          //creates number of horizontal diamonds
          for (int h =1; h <= diamonds; h++) {
            //creates half upper left half of diamond
            for ( int j = 0; j <= dwidth; j++) {
              if (j == i) {
                System.out.print("#");
              }
              else if (j > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            //creates upper right half of diamond
            for (int k = 0; k <= dwidth; k++) {
              if ( k == dwidth - i ) {
                System.out.print("#");
              }
              else if ( k < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          //creates remaining diamond width
          if(remdiamonds <= dwidth) {
            for ( int n = 0; n < remdiamonds; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          else if (remdiamonds > dwidth) {
            for ( int n = 0; n <= dwidth; n++) {
              if (n == i) {
                System.out.print("#");
              }
              else if (n > (dwidth-i)){
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
            for (int p = 0; p <= (remdiamonds - dwidth); p++) {
              if ( p == dwidth - i ) {
                System.out.print("#");
              }
              else if ( p < i) {
                System.out.print("@");
              }
              else {
                System.out.print("-");
              }
            }
          }
          //for (int m = 1; m <= twidth - diamonds*dwidth) {

          //}
          System.out.println(" ");
        }

        //SECOND ROW - Bottom half of diamond 

        //creates vertical rows for bottom half of diamond
        for( int i = 0; i <= remhdiamonds-dwidth; i++) {
          //creates user specified number of diamonds 
            for ( int h =1; h <= diamonds; h++) {
              //creates lower left half of diamond 
              for ( int k = 0; k < dwidth; k++) {
                if ( k == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( k > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
              //creates lower right half of diamond
              for ( int j = 0; j < dwidth; j++) {
                if (j == i) {
                  System.out.print("#");
                }
                else if (j < (dwidth-i)){
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
          //creates remaining width for diamond
            if(remdiamonds <= dwidth) {
              for ( int n = 0; n < remdiamonds; n++) {
                if ( n == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( n > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
            else if (remdiamonds > dwidth) {
              for ( int n = 0; n <= dwidth; n++) {
                if ( n == dwidth - i ) {
                  System.out.print("#");
                }
                else if ( n > i) {
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
              for ( int n = 0; n <= (remdiamonds-dwidth); n++) {
                if (n == i) {
                  System.out.print("#");
                }
                else if (n < (dwidth-i)){
                  System.out.print("@");
                }
                else {
                  System.out.print("-");
                }
              }
            }
            //for (int m = 1; m <= twidth - diamonds*dwidth) {

            //}
            System.out.println(" ");
          }
      }
    }
    
  }
}