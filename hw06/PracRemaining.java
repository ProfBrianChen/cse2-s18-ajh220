////////////////////////////////

// UPPER HALF X with diamond behind

//  THIS ONE WORKS

// Creates both X with proper stripe and diamond


import java.util.Scanner;

public class PracRemaining {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter height int: ");
    int height = scan.nextInt();
    
    System.out.println("Enter total width int: ");
    int twidth = scan.nextInt();
    
    System.out.println("Enter Argyle diamond width: ");
    int dwidth = scan.nextInt();
    
    System.out.println ("Enter a positive odd integer for the width of the argyle center stripe: ");
    int stripe = scan.nextInt();
    
    int diamonds = (twidth/(dwidth*2));
    int hdiamonds = (height/(dwidth*2));
    
    int remdiamonds = twidth%(dwidth*2);
    int remhdiamonds = height%(dwidth*2);
    
    System.out.println(diamonds);
    System.out.println(remdiamonds);
    
    int z = 0;
    int y = 0;
    int x = 0;
    
    for (int d=1; d <= diamonds; d++) {
    
      //TOP HALF

      //creates height of top half of diamond
      for (int k = 1; k <= dwidth; k++) {
        //creates number of horizontal diamonds
        for ( int j =1; j <= diamonds; j ++) {

          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;

          //Upper right half

          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        if ( remdiamonds > dwidth) {
          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
          
          //Upper right half

          for ( int i = 1; i <= remdiamonds-dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        else if ( remdiamonds > 0 && remdiamonds <= dwidth) {
          for ( int i = 1; i <= remdiamonds-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        System.out.println("");
      }

      //BOTTOM HALF

      z=0;
      y=0;
      x=0;

      //creates height of bottom half of diamond
      for (int k = 1; k <= dwidth; k++) {
        //creates number of horizontal diamonds
        for ( int c =1; c <= diamonds; c ++) {

          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;

          //creates lower right half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && x == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              x++;
            }
            else if ( i <= dwidth - k && i < k-stripe/2) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i <= (dwidth-stripe/2) - k && i >= k + stripe/2) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
        }
        
        if (remdiamonds > dwidth) {
          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;
          
          //creates lower right half of diamond width which accounts for stripe width
          for ( int i = 1; i <= remdiamonds-dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && x == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              x++;
            }
            else if ( i <= dwidth - k && i < k-stripe/2) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i <= (dwidth-stripe/2) - k && i >= k + stripe/2) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
        }
        else if ( remdiamonds > 0 && remdiamonds <= dwidth) {
          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= remdiamonds; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;
        }
        System.out.println("");
        z =0;
        y =0;
        x =0;
      }
    }
    if ( remhdiamonds > dwidth ) {
      
      //TOP HALF

      //creates height of top half of diamond
      for (int k = 1; k <= dwidth; k++) {
        //creates number of horizontal diamonds
        for ( int j =1; j <= diamonds; j ++) {

          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;

          //Upper right half

          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        if ( remdiamonds > dwidth) {
          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
          
          //Upper right half

          for ( int i = 1; i <= remdiamonds-dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        else if ( remdiamonds > 0 && remdiamonds <= dwidth) {
          for ( int i = 1; i <= remdiamonds-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        System.out.println("");
      }
      
      //BOTTOM HALF

      z=0;
      y=0;
      x=0;

      //creates height of bottom half of diamond
      for (int k = 1; k <= remhdiamonds-dwidth; k++) {
        //creates number of horizontal diamonds
        for ( int c =1; c <= diamonds; c ++) {

          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;

          //creates lower right half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && x == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              x++;
            }
            else if ( i <= dwidth - k && i < k-stripe/2) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i <= (dwidth-stripe/2) - k && i >= k + stripe/2) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
        }
        
        if (remdiamonds > dwidth) {
          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;
          
          //creates lower right half of diamond width which accounts for stripe width
          for ( int i = 1; i <= remdiamonds-dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && x == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              x++;
            }
            else if ( i <= dwidth - k && i < k-stripe/2) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i <= (dwidth-stripe/2) - k && i >= k + stripe/2) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
        }
        else if ( remdiamonds > 0 && remdiamonds <= dwidth) {
          //creates lower left half of diamond width which accounts for stripe width
          for ( int i = 1; i <= remdiamonds; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && z == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              z++;
            }
            else if (i > k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i > k  && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          z=0;
          y=0;
          x=0;
        }
        System.out.println("");
        z =0;
        y =0;
        x =0;
      }
      
      
    }
       
    
    else if ( remhdiamonds > 0 && remhdiamonds < dwidth ) {
      //TOP HALF

      //creates height of top half of diamond
      for (int k = 1; k <= remhdiamonds; k++) {
        //creates number of horizontal diamonds
        for ( int j =1; j <= diamonds; j ++) {

          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;

          //Upper right half

          for ( int i = 1; i <= dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        if ( remdiamonds > dwidth) {
          //creates upper left half

          for ( int i = 1; i <= dwidth-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
          
          //Upper right half

          for ( int i = 1; i <= remdiamonds-dwidth; i++) {
            if ( i >= dwidth - (k-1) - stripe/2 && x == 0) {
              for ( int n = 1; n <=stripe/2; n++){
                System.out.print("#");
              }
              x++;
            }
            else if (i <k && i <= dwidth-k-stripe/2){
              System.out.print("@");
            }
            else if ( i < dwidth - (k-1) - stripe/2) {
              System.out.print("-");
            }

            if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k <= dwidth-stripe/2) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            } 
            else if (i == dwidth - (k-1) && stripe/2+1 > dwidth -i) {
              for (int  p=i; p<= dwidth; p++) {
                System.out.print("#");
              }
            }
            else if (i == dwidth - (k-1) && (stripe/2+1) <= dwidth - i && k > dwidth-stripe/2) {
              for (int q =k; q<=dwidth; q++) {
                System.out.print("#");
              }
            }
            else if (i<k && i>dwidth - (k-1) + stripe/2) {
              System.out.print("@");
            }
            else if (i > dwidth - (k-1) + stripe/2) {
              System.out.print("-");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        else if ( remdiamonds > 0 && remdiamonds <= dwidth) {
          for ( int i = 1; i <= remdiamonds-stripe/2; i++) {
            if ( i >= (k - stripe/2) && i<k && z == 0) {
              for ( int n = 1; n <=k-i; n++){
                System.out.print("#");
              }
              z++;
            }
            else if ( i < (k-stripe/2) && i > dwidth - (k-1)) {
              System.out.print("@");
            }
            else if ( i < (k-stripe/2)) {
              System.out.print("-");
            }

            if (i == k) {
              for (int m = 1; m <= stripe/2+1; m++) {
                System.out.print("#");
              }
            }
            else if ( i > k && i > dwidth-stripe/2 - (k-1)) {
              System.out.print("@");
            }
            else if (i > k) {
              System.out.print("-");
            }
          }
          if( k > dwidth-stripe/2) {
            for( int q = k; q<= dwidth; q++) {
              System.out.print("#");
            }
          }
          x =0;
          y =0;
          z =0;
        }
        System.out.println("");
      }
    }
  }
}