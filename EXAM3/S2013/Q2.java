

public class Q2 {
  public static int findLongest(int[][] input) {
    int longest = input[0].length;
    for (int i=0; i<input.length; i++) {
      if (input[i].length>longest) {
        longest = input[i].length;
      }
    }
    return longest;
  }
  public static void main(String[] args) {
    int[][] A = {
      {3, 2, 2},
      {7},
      {0, 3},
      {2, 0, 0, 0, 0},
    };
    int longest = findLongest(A);
    System.out.println(longest);
  }
}