//////////////////////////////////////////////////////////////////////////////////////////////////////

import java.util.Scanner;

public class Q1 {
  public static int[] makeZero(int val) {
    int[] zeros = new int[val];
    for (int i=0; i<val; i++) {
      zeros[i] = 0;
    }
    return zeros;
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter an integer: ");
    int input = scan.nextInt();
    
    int[] zeros = makeZero(input);
    for (int i=0; i<zeros.length; i++) {
      System.out.println(zeros[i]);
    }
  }
}