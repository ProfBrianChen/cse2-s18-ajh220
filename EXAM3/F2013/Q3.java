

public class Q3 {
  public static void main(String[] args) {
    int [][] A = {
      {3, 1, 4},
      {2, 5, 7, 1},
      {2},
      {1, 4},
    };
    int shortest = A[0].length;
    int shortestIndex = 0;
    for (int i=0; i<A.length; i++) {
      if (A[i].length < shortest) {
        shortest = A[i].length;
        shortestIndex = i;
      }
    }
    System.out.println(shortestIndex);
  }
}