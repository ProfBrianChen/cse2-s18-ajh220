

public class Q4 {
  public static int[] overlap(int[] x, int[] y) {
    int sameCounter =0;
    int[] overlap;
    if (x.length > y.length) {
      overlap = new int[x.length];
    }
    else {
      overlap = new int[y.length];
    }
    overlap[0] = sameCounter;
    for (int i=0; i< x.length; i++) {
      for (int j=0; j< y.length; j++) {
        if (x[i] == y[j]) {
          sameCounter++;
          overlap[sameCounter] = x[i];
          overlap[0] = sameCounter;
        }
      }
    }
    return overlap;
  }
  public static void main(String[] args) {
    int[] x = {1, 10, 3, 7};
    int[] y = {2, 3, 4, 5, 10, 6};
    int[] same = overlap(x,y);
    
    for (int i=0; i<same.length; i++) {
      System.out.println(same[i]);
    }
  }
}