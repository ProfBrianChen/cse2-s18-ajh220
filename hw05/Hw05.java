/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////                    CSE2 HW 05
//////////////////////////////////////       Adam Hilal             March 5 2018
// 
//   accept correct user input data about a class


import java.util.Scanner;

public class Hw05 {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    //define counting variables
    int i = 0;
    char j = 'a';
    
    //infinte loop for course number
    for(int inf = 0; inf<(inf +1); inf++){
      scan = new Scanner(System.in);
      System.out.println("Enter the course number: ");
      
      //check for int value
      if(scan.hasNextInt()) {
        System.out.println("correct type");
        break;     
      }
      else{
        System.out.println("Unacceptable value, please enter an int");
      }
    }
    
    //infinte loop for department name
     for(int inf = 0; inf<(inf +1); inf++){
      scan = new Scanner(System.in);      
      System.out.println("Enter the Department name: ");
      String Dept = scan.nextLine();
      
       //for loop to check every charater in string from user to make sure it contains only letters
      for( i = 0; (i+1) <= (Dept.length()); i++) {
        for( j= 'a'; j<= 'z'; j++) {
         if (j == Dept.charAt(i)){
           break;
         }
         else if (j == 'z' && Dept.charAt(i) != 'z') {
           System.out.println("Unacceptable department name.  Please enter name as a string in all lower case letter");
           break;
         }
        }
        if (j == 'z' && Dept.charAt(i) != 'z') {
          break;
        }
        else if ((i+1) == Dept.length()){
          break;
        }
      }
       if ((i+1)== Dept.length()){
         break;
       }
     }
     
    //infinite loop checking for int value for class meeting time
     for(int inf = 0; inf<(inf +1); inf++){
      scan = new Scanner(System.in);
      System.out.println("How many times a week does the class meet? ");
      if(scan.hasNextInt()) {
        break;     
      }
      else{
        System.out.println("Unacceptable value, please enter an int");
      }
    }
    
    //infinite loop checking for double value for starting class time
    for(int inf = 0; inf<(inf +1); inf++){
      scan = new Scanner(System.in);
      System.out.println("What hour does the class start? ");
      if (scan.hasNextInt()){
        int hour = scan.nextInt();
        if(hour>=0 && hour <= 12) {
          System.out.println("At what minute does the class start? ");
          if(scan.hasNextInt()) { 
            int minute = scan.nextInt();
            if(minute>=0 && minute <= 60) {
              break;
            }
            else{
            System.out.println("Unacceptable value, please enter the hours and the minutes in int form");
            }
          }
          else{
          System.out.println("Unacceptable value, please enter the hours and the minutes in int form");
          }
        }
        else{
        System.out.println("Unacceptable value, please enter the hours and the minutes in int form");
        }
      }
      
      else{
        System.out.println("Unacceptable value, please enter the hours and the minutes in int form");
      }
    }
    
    //infinite loop checking for only letters in professor's name
    for(int inf = 0; inf<(inf +1); inf++){
      scan = new Scanner(System.in);      
      System.out.println("Enter the Professor's name: ");
      String Dept = scan.nextLine();
      
      for( i = 0; (i+1) <= (Dept.length()); i++) {
        for( j= 'a'; j<= 'z'; j++) {
         if (j == Dept.charAt(i)){
           break;
         }
         else if (j == 'z' && Dept.charAt(i) != 'z') {
           System.out.println("Unacceptable Professor name.  Please enter name as a string in all lower case letter");
           break;
         }
        }
        if (j == 'z' && Dept.charAt(i) != 'z') {
          break;
        }
        else if ((i+1) == Dept.length()){
          break;
        }
      }
       if ((i+1)== Dept.length()){
         break;
       }
     }

    
    
    
    
    
  }
}