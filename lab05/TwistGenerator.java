////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////           CSE2 Lab 5
////////////////////////////////////                Adam Hilal                March 5, 2018
///
//
//Create a program to output a specific pattern based on a user integer input


import java.util.Scanner;

public class TwistGenerator {
  public static void main(String[] args ) {
    Scanner scan = new Scanner(System.in);
    
    //infite loop in case user does not enter proper value
    while (true) {
      
      //reset scan value
      scan = new Scanner(System.in);
      System.out.println("Enter an integer value greater than 0: ");
      
      //check for int
      if(scan.hasNextInt()) {
        int val = scan.nextInt();
        
        //check for positive int
        if (val > 0) {
          
          //create upper line sequence
          if(val>=3) {
            for (int i = 3; i <= val; i+=3) {
              System.out.print("\\ /");
            }
          }
          if(val%3 == 1){
            System.out.print("\\");
          }
          else if(val%3 == 2){
            System.out.print("\\ ");
          }
          System.out.print("\n");

          //create middle line sequence
          if (val >=3){
            for (int i = 3; i <= val; i+=3) {
              System.out.print(" X ");
            }
          }

          if(val%3 == 1){
            System.out.print(" ");
          }
          else if(val%3 == 2){
            System.out.print(" X");
          }
          System.out.print("\n");

          //create lower line sequence
          if (val >= 3 ) {
            for (int i = 3; i <= val; i+=3) {
              System.out.print("/ \\");
            }
          }

          if(val%3 == 1){
            System.out.print("/");
          }
          else if(val%3 == 2){
            System.out.print("/ ");
          }
          System.out.print("\n");
          
          //after completion, break out of loop
          break;
        }
      else {
        System.out.println("Enter a positive integer");
      }
      }
      
      else {
        System.out.println("Enter a positive integer");
      }
      
      
      
    }
  }
}