///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////            CSE 2 HW07

/////                       Adam Hilal                      3/26/2018


import java.util.Scanner;

public class StringAnalysis {
  public static void main(String[] args) {
   
    //declare and initialize values
    String input = "";
    String eval = "";
    int TBE = 0;
    boolean finished = false;
    
    while (true) {
    
      Scanner scan = new Scanner(System.in);
      
      //accept string value
      System.out.println("Enter a string to be evaluated: ");
      input = scan.nextLine();

      //accept responses until proper respones are entered
      while (true) {
        scan = new Scanner(System.in);
        System.out.println("Do you want to evaluate all the characters in the string? Enter \"yes\" or \"no\" ");
        eval = scan.nextLine();
        if (eval.equals("yes") || eval.equals("no")) {
          break;
        }
        else {
          System.out.print("invalid answer given");
        }
      }
      //call program to evaluate all answers
      if (eval.equals("yes")) {
        finished = FullEval(input);
        if (finished == true) {
          break;
        }
        else {
          System.out.println("invalid string entered, please repeat the process");
        }
      }
      //call program to evaluate select number of characters
      else if (eval.equals("no")) {
        System.out.print("How many characters would you like to evaluate: ");
        TBE = scan.nextInt();
        finished = PartEval(input, TBE);
        if (finished == true) {
          break;
        }
        else {
          System.out.println("invalid string entered, please repeat the process");
        }
      }
      
    }
    
    System.out.println("The string evaluated contains all letters");
    
  }
  
  
  public static boolean FullEval(String input) {
    
    int length = input.length();
    for (int i = 0; i<length; i++) {
      
      char evalchar = input.charAt(i);
      //total input is acceptable
      if (i == length && (evalchar >= 'a' && evalchar <= 'z' || evalchar >= 'A' && evalchar <= 'Z')) {
        return true;
      }
      //inbetweem values are acceptable
      else if (evalchar >= 'a' && evalchar <= 'z' || evalchar >= 'A' && evalchar <= 'Z') {
      }
      //non letter found
      else {
        return false;
      }
    }
    return true;
  }
  public static boolean PartEval(String input, int TBE) {
    
    for (int i = 0; i<TBE; i++) {
      
      char evalchar = input.charAt(i);
      //total partial input is acceptable
      if (i == TBE && (evalchar >= 'a' && evalchar <= 'z' || evalchar >= 'A' && evalchar <= 'Z')) {
        return true;
      }
      //inbetween values are acceptable
      else if (evalchar >= 'a' && evalchar <= 'z' || evalchar >= 'A' && evalchar <= 'Z') {
      }
      //non letter found
      else {
        return false;
      }
    }
    return true;
  }
}