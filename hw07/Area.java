/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////               CSE 2   HW 07

////                                  Adam Hilal                   3/26/2018

// Calculates the area of a shape based on user choice of shape and dimension inputs


import java.util.Scanner;

public class Area {
  public static void main(String[] args) {
    
    //call input check method to determine if valid shape is provided
    String shape = inputCheck();
    Double Area = 0.0;
    
    //choose a method based on shape entered
    if (shape.equals("rectangle")) {
        Area  = RectArea();
      }
      else if (shape.equals("triangle")) {
        Area = TriangleArea();
        System.out.println("triangle");
      }
      else if (shape.equals("circle")) {
        Area = CircleArea();
      }
    
    System.out.println("The area is " + Area);
  }
  public static String inputCheck() {
    Scanner scan = new Scanner(System.in);
    // run input acceptance until proper string is given
    while (true) {
      
      System.out.println("Enter a shape: ");
      String shape = scan.nextLine();
      
      if (shape.equals("rectangle")) {
        return shape;
      }
      else if (shape.equals("triangle")) {
        return shape;        
      }
      else if (shape.equals("circle")) {
        return shape;
      }
      else {
        System.out.println("invalid input entered, enter either \"rectangle\", \"triangle\", or \"circle\"");
      }
      
    }
  }
  public static Double RectArea() {
    
    Double height = 0.0;
    Double width = 0.0;
    
    // run dimension acceptance until proper values are entered
    while (true) {
      
      Scanner scan = new Scanner(System.in);
      
      System.out.println("Enter rectangle height as a double: ");
      if (scan.hasNextDouble()) {
        height = scan.nextDouble();
      }
      else {
        System.out.println("Invalid type entered");
        continue;
      }
      
      System.out.println("Enter rectangle width as a double: ");
      if (scan.hasNextDouble()) {
        width = scan.nextDouble();
        break;
      }
      else {
        System.out.println("Invalid type entered");
        continue;
      }
    }
    
    //calculate and return area value to main method
    Double Area = height*width;
    return Area;
  }
  
  //repeat same process as rectangle but with different calculations
  public static Double TriangleArea() {
    
    Double height = 0.0;
    Double width = 0.0;
    
    while (true) {
      
      Scanner scan = new Scanner(System.in);
      
      System.out.println("Enter triangle height as a double: ");
      if (scan.hasNextDouble()) {
        height = scan.nextDouble();
      }
      else {
        System.out.println("Invalid type entered");
        continue;
      }
      
      System.out.println("Enter triangle width as a double: ");
      if (scan.hasNextDouble()) {
        width = scan.nextDouble();
        break;
      }
      else {
        System.out.println("Invalid type entered");
        continue;
      }
    }
    Double Area = .5*height*width;
    return Area;
  }
  public static Double CircleArea() {
    
    Double radius = 0.0;
    
    while (true) {
      
      Scanner scan = new Scanner(System.in);
      
      System.out.println("Enter circle radius as a double: ");
      if (scan.hasNextDouble()) {
        radius = scan.nextDouble();
        break;
      }
      else {
        System.out.println("Invalid type entered");
        continue;
      }     
    }
    //use math.pi to be more precise
    Double Area = Math.PI*radius*radius;
    return Area;
  }
}