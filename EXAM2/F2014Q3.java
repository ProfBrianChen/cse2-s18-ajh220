//////////

import java.util.Scanner;

public class F2014Q3 {
  public static void main(String[] args) {
    while(true){
      Scanner scan = new Scanner(System.in);
      System.out.println("Enter an integer: ");
      int val1 = scan.nextInt();
      System.out.println("Enter an integer greater than " + val1 + ": ");
      int val2 = scan.nextInt();
      if (val2>val1){
        int result = sum(val1,val2);
        break;
      }
      else{
        System.out.println("Sorry, you entered " + val2 + " <= " + val1 + "; try again");
      }
      
    }
  }
  public static int sum( int x, int y) {
    int j = x;
    for (int i =x; i<=y; i++) {
      if ( i > x ) {
        j += i;
      }
      System.out.print(i + " + ");
    }
    System.out.println(" = " + j);
    return j;
  }
}