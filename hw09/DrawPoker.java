///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////                 CSE2 HW 09
///////////////             Adam Hilal                    4/16/18

//// program creates a deck of cards, randomly shuffles them, and then ranks the hands and declares a winner

import java.util.Random;

public class DrawPoker {
  public static void shuffle (int[] deck) {
    Random rand = new Random();
    //shuffle deck
    for (int i=0; i<52; i++) {
      int temp = deck[i];
      int replace = rand.nextInt(52);
      deck[i] = deck[replace];
      deck[replace] = temp;
    }
  }
  public static int[] selection (int[] deck) {
    Random rand = new Random();
    int[] Chosen = new int[10];
    
    for (int i=0; i<Chosen.length; i++) {
      boolean used = true;
      while (true) {
        Chosen[i] = rand.nextInt(52);
        if ( i==0) {
          break;
        }
        else {
          //determine whether or not a card had already been drawn
          for (int j=0; j<i; j++) {
            if (Chosen[i] == Chosen[j]) {
              break;
            }
            else if(j==i-1){
              used = false;
            }
          }
        }
        if (used == false) {
          break;
        }
      }
    }
    return Chosen;
  }
  public static String SuitAssign( int decknumber) {
    int cardnumber = decknumber/13;
    String suit = "";
    switch (cardnumber) {
      case 0:
        suit = "diamonds";
        break;
      case 1:
        suit = "clubs";
        break;
      case 2:
        suit = "hearts";
        break;
      case 3:
        suit = "spades";
        break;
    }
    
    return suit;
  }
  public static String ValAssign (int decknumber) {
    int value = decknumber%13;
    String card = "";
    switch (value) {
      case 0:
        card = "Ace";
        break;
      case 1:
        card = "2";
        break;
      case 2:
        card = "3";
        break;
      case 3:
        card = "4";
        break;
      case 4:
        card = "5";
        break;
      case 5:
        card = "6";
        break;
      case 6:
        card = "7";
        break;
      case 7:
        card = "8";
        break;
      case 8:
        card = "9";
        break;
      case 9:
        card = "10";
        break;
      case 10:
        card = "Jack";
        break;
      case 11:
        card = "Queen";
        break;
      case 12:
        card = "King";
        break;
    }
    return card;
  }
  
  public static boolean pair (String[] hand) {
    for (int j=0; j<hand.length;j++) {
      for (int i=j+1; i<hand.length; i++) {
        if (hand[j].equals(hand[i])) {
          return true;
        }
      }
    }
    return false;
  }
  public static boolean ThreeKind (String[] hand) {
    for (int j=0; j<hand.length;j++) {
      for (int i=j+1; i<hand.length; i++) {
        if (hand[j].equals(hand[i])) {
          for (int k=i+1; k<hand.length; k++) {
            if (hand[k].equals(hand[i])) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  public static boolean flush (String[] hand) {
    int counter = 0;
    for(int i=1; i<hand.length; i++) {
      if(hand[0].equals(hand[i])) {
        counter++;
      }
    }
    if (counter==5) {
      return true;
    }
    return false;
  }
  public static boolean fullHouse(String hand[]) {
    String val = "0";
    for (int j=0; j<hand.length;j++) {
      for (int i=j+1; i<hand.length; i++) {
        if (hand[j].equals(hand[i])) {
          val = hand[j];
        }
      }
    }
    if (!val.equals("0")) {
      for (int j=0; j<hand.length;j++) {
        for (int i=j+1; i<hand.length; i++) {
          if (hand[j].equals(hand[i]) && !hand[j].equals(val)) {
            for (int k=i+1; k<hand.length; k++) {
              if (hand[k].equals(hand[i])) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public static void main(String[] args) {
    //create the deck of cards
    int[] deck = new int[52];
    for (int i = 0; i<52; i++) {
      deck[i] = i; 
    }
    shuffle(deck);
    
    int[] Selected = new int[10];
      Selected = selection(deck);
    
    String[] player1 = new String[5];
    String[] player2 = new String[5];
    int x = 0;
    int y = 0;
   
    for (int j=1; j<=10; j++) {
      if (j%2 == 1) {
        player1[x] = ValAssign(Selected[j-1]) + " of " + SuitAssign(Selected[j-1]);
        x++;
      }
      else if (j%2 == 0) {
        player2[y] = ValAssign(Selected[j-1]) + " of " + SuitAssign(Selected[j-1]);
        y++;
      }
    }
    
    //print hands
    System.out.println("Player 1:");
    for (int k = 0; k<5; k++) {
      System.out.println(player1[k]);
      
    }
    System.out.println("");
    
    System.out.println("Player 2:");
    for (int l = 0; l<5; l++) {
      System.out.println(player2[l]);
    }
    
    //restructure arrays for evaluation
    String[] player1suit = new String[5];
    String[] player2suit = new String[5];
    
    String[] player1card = new String[5];
    String[] player2card = new String[5];
    
    x=0;
    y=0;
    for (int j=1; j<=10; j++) {
      if (j%2 == 1) {
        player1suit[x] = SuitAssign(Selected[j-1]);
        player1card[x] = ValAssign(Selected[j-1]);
        x++;
      }
      else if (j%2 == 0) {
        player2suit[y] = SuitAssign(Selected[j-1]);
        player2card[y] = ValAssign(Selected[j-1]);
        y++;
      }
    }
    
    
    //Evaluate player's hand for pairs
    boolean pair1 = pair(player1card);
    boolean pair2 = pair(player2card);
    
    //evaluate player's hand for three of a kind
    boolean threeKind1 = ThreeKind(player1card);
    boolean threeKind2 = ThreeKind(player2card);
    
    //evaluate for flush
    boolean flush1 = flush(player1suit);
    boolean flush2 = flush(player2suit);
    
    //evaluate for full house
    boolean full1 = fullHouse(player1card);
    boolean full2 = fullHouse(player2card);
    
    int counter1 = 0;
    int counter2 = 0;
    
    if (pair1 == true) {
      counter1++;
    }
    if (threeKind1 == true) {
      counter1++;
    }
    if (flush1 == true) {
      counter1++;
    }
    if (full1 == true) {
      counter1++;
    }
    if (pair2 == true) {
      counter2++;
    }
    if (threeKind2 == true) {
      counter2++;
    }
    if (flush2 == true) {
      counter2++;
    }
    if (full2 == true) {
      counter2++;
    }
    
    if (counter1 > counter2) {
      System.out.println("Player 1 wins");
    }
    else if ( counter2 < counter1 ) {
      System.out.println("Player 2 wins");
    }
    else if (counter2 == counter1) {
      System.out.println("There is a tie");
    }
  }
}