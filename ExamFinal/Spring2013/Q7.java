//////////////////////////////////////////////////////////////////////////////////////////////////

import java.util.Random;

public class Q7 {
  public static void swapMax( int[] A, int K) {
    int high = 0;
    int position = K;
    for (int i=K+1; i< A.length; i++) {
      if (A[i] > high) {
        high= A[i];
        position = i;
      }
    }
    int temp = A[K];
    A[K] = high;
    A[position] = temp;
    
  }
  public static void main(String[] args) {
    Random rand = new Random();
    
    int[] myArray = new int[10];
    for (int i=0; i<10; i++) {
      myArray[i] = rand.nextInt(10);
      System.out.println(myArray[i]);
    }
    System.out.println("");
    swapMax(myArray, 3);
    for (int k=0; k<10; k++) {
      System.out.println(myArray[k]);
    }
      
    
  }
}