//////////////////////////////////////////

import java.util.Random;

public class Q11 {
  public static void circleNudge( int[] A, int K) {
    int [] temp = new int[A.length + K];
    for (int i=0; i<A.length; i++) {
      temp[i+K] = A[i];
    }
    for (int j=0; j< K; j++) {
      temp[j] = temp[A.length+j];
    }
    for (int m=0; m<A.length; m++) {
      A[m] = temp[m];
    }
  }
  
  public static void printArray ( int[] A) {
    for (int i=0; i<A.length; i++) {
      System.out.println(A[i]);
    }
    System.out.println("");
  }
  public static void main(String[] args) {
    int[] myArray = { 0, 1, 2, 3, 4};
    
    printArray(myArray);
    circleNudge(myArray, 2);
    printArray(myArray);
  }
}