/////////////////////////////////////////////////////////////////////////////

import java.util.Random;

public class Q6 {
  public static void reverse() {
    Random rand = new Random();
    
    //populate array with random values
    int[] myArray = new int[rand.nextInt(10) + 5];
    for (int k=0; k< myArray.length; k++) {
      myArray[k] = rand.nextInt(10);
    }
    
    printArray(myArray);
    
    int temp = 0;
    for (int i=0; i<myArray.length/2; i++) {
      temp = myArray[i];
      myArray[i] = myArray[(myArray.length-1)-i];
      myArray[(myArray.length-1)-i] = temp;
    }
    
    printArray(myArray);
  }
  
  public static void printArray(int[] myArray) {
    for (int i=0; i<myArray.length; i++) {
      System.out.println(myArray[i]);
    }
    System.out.println("");
  }
  
  public static void main(String[] args) {
    reverse();
  }
}