/////////////////////////////////////////////////////

public class Q5 {
  public static void main(String[] args) {
    
    int i = 5;
    int counter = 1;
    while (i > 0) {
      int j = 0;
      while (j < i) {
        int k = 0;
        while ( k < i-j) {
          System.out.print(i);
          k++;
        }
        while (k >= i-j && k < i) {
          System.out.print(counter);
          k++;
        }
        System.out.println("");
        j++;
      }
      counter++;
      i--;
    }
  }
}