///////////////////////////////////////////////////////////////////////////////

public class Q5 {
  public static char[][] starry(int dimension) {
    char[][] star = new char[dimension][];
    for (int i=0; i<dimension; i++) {
      star[i] = new char[dimension];
      for (int k=0; k<dimension; k++) {
        if (k==i || k==(dimension-1)-i) {
          star[i][k]='*';
        }
        else {
          star[i][k] =(char)('0'+i);
        }
      }
    }
    return star;
  }
  public static void printArray(char[][] Array) {
    for (int i=0; i<Array.length; i++) {
      for (int k=0; k<Array[i].length; k++) {
        System.out.print(Array[i][k]);
      }
      System.out.println("");
    }
  }
  public static void main(String[] args) {
    char[][] pleaseWork = starry(10);
    printArray(pleaseWork);
  }
}