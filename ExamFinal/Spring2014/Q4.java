////////////////////////////////////////////////////////


public class Q4 {
  public static char[] stringToChars(String myString) {
    int spaceCount = 0;
    int counter = 0;
    for (int i=0; i<myString.length(); i++) {
      if (myString.charAt(i) == ' ') {
        spaceCount++;
      }
    }
    char[] myArray = new char[myString.length()-spaceCount];
    for (int k=0; k<myString.length(); k++) {
      if (myString.charAt(k) == ' ') {
        continue;
      }
      myArray[counter] = myString.charAt(k);
      counter++;
    }
    return myArray;
  }
  public static void printArray(char[] Array) {
    for (int i=0; i<Array.length; i++) {
      System.out.print(Array[i]);
    }
  }
  public static void main(String[] args) {
    String yep = "This is a string";
    char[] finished = stringToChars(yep);
    printArray(finished);
  }
}