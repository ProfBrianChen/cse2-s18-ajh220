

public class Q10 {
  public static void insertionSort(int[] myArray) {
    for (int i=1; i<myArray.length; i++) {
      int safe = i;
      while (myArray[i]<myArray[i-1]) {
        int temp = myArray[i-1];
        myArray[i-1] = myArray[i];
        myArray[i] = temp;
        i--;
        if (i==0) {
          break;
        }
      }
      i= safe;
    }
  }
  public static void main(String[] args) {
    int[] myArray = { 2, 5, 1, 9, 3, 6, 0, 4, 8, 7};
    insertionSort(myArray);
    for(int i=0; i<myArray.length; i++) {
      System.out.print(myArray[i]);
    }
  }
}