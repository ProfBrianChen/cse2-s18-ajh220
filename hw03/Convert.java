//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////    CSE2 HW3 Program 1
/////////////////////////////////////  Adam Hilal   February 12,2018


import java.util.Scanner;      //create scanner for program

public class Convert {
  public static void main(String[] args) {
    
    Scanner scanner1 = new Scanner(System.in);      //define scanner to take inputs
    
    
    //take inputs from user
    
    System.out.print("Enter the affected area in acres: ");
    double acres = scanner1.nextDouble();
      
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = scanner1.nextDouble();
    
    //43560 sq ft per acre
    
    double sqfeet = acres*43560;
    
    //converting rainfall from ft to in
    
    double rainfallft = rainfall/12;
    
    //CATCHMENT AREA(ft2) * RAINFALL (ft) * 7.48 gal/ft3 = TOTAL RAINWATER (gal)
    
    double gallons = sqfeet*rainfallft*7.48;
    
    //9.08169e-13 cubic miles per gallon
    
    double cubicmiles = gallons*(9.08169e-13);
    
    // cut off number after 7 digits
    double cubicmiles2 = cubicmiles*10e7;
    int cubicmiles3 = (int) cubicmiles2;
    double cubicmiles4 = cubicmiles3/10e7;
    
    //print result
    System.out.println(cubicmiles4 + " cubic miles");
  }
}