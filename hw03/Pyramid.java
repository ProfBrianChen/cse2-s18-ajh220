///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////    CSE2 HW3 Program 2
///////////////////////////////////////    Adam Hilal 2/12/2018


import java.util.Scanner;         //create scanner for program


public class Pyramid {
  public static void main(String[] args) {
    
    Scanner scanner1 = new Scanner(System.in);       //define scanner to take inputs
    
    //take inputs from user
    
    System.out.print("The square side of the pyriamid is (instert length): ");
    double sidelength = scanner1.nextDouble();
    
    System.out.print("The height of the pyramid is (insert height): ");
    double height = scanner1.nextDouble();
    
    //Volume = (Length*Width*Height) / 3
    
    double Volume = ((Math.pow(sidelength , 2))*height)/3;
    
    //print results
    
    System.out.println("The volume of the pyramid is: " + Volume);
    }
  }