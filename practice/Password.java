//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



import java.util.Scanner;

public class Password {
  public static void main(String[] args) {
    Scanner X = new Scanner(System.in);
    
    //Accept pass word from user
    System.out.print("Please enter your password:");
    String password = X.nextLine();
    
    //verify password
    System.out.print("Please re-enter your password for verification:");
    String password2 = X.nextLine();
    
    //define
    char pos1 = 'a';
    char y= 'A';
    
    //check if passwords enter are equal
    if ( password.equals(password2) ) {
      System.out.println("Password accepted");
      
      //measure password length
      int passlength = password.length();
      System.out.println("Password length is: " + passlength);
      
      //analyze each character of password starting with first letter
      for ( int counter = 0; (counter+1) <=passlength; counter++) {
        
        //run through alphabet comparing the letter of the password to the letter of the alphabet
        for ( char x ='a'; x<='z'; x++) {
          
          //if letters match than print to screen
          if (x== password.charAt(counter)) {
            pos1 = x;
            System.out.print(pos1);
            
            //break out of for loop to move on to next letter in the password
            break;  
          }
          
          //after having run through all lowercase letters, check uppercase letters
          else if (x == 'z' && x != password.charAt(counter)){
            for ( char y = 'A'; y<='Z'; y++) {
              if(y == password.charAt(counter)) {
                pos1 = y;
                System.out.print(pos1);
                break;
              }
            }
          }
          
          else if (x == 'z' && y == 'Z' && x != password.charAt(counter)) {
            for (char w = '0'; w<='9'; w++) {
              if (w == password.charAt(counter)){
                pos1=w;
                System.out.print(pos1);
                break;
              }
            }
          }
        }      
      }
    }
    else {
      System.out.println("Passwords entered do not match");
    }
   System.out.println(" ");
  }
}