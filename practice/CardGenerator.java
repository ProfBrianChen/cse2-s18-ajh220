////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////   CSE2 Card Generator   LAB 4
///////////////////////////////////   Adam Hilal  February 2, 2018
//////////////////////////////////      


public class CardGenerator {
  public static void main(String[] args) {
    
    //define and initialize suit to be used later
    String suit = "";
    
    //pick random integer from 1-52
    int decknumber = (int)(Math.random()*52)+1;
    
    //assigns suit to randomly selected card
    if ( decknumber <= 13) {
      suit = "Diamonds";
      
    }
    else if ( decknumber > 13 && decknumber <= 26) {
     suit = "Clubs";
      
    }
    else if (decknumber > 26 && decknumber <= 39 ) {
      
     suit = "Hearts";
      
    }
    else if (decknumber > 39) {
     suit = "Spades";
      
    }
    
    //define and initialize cardtype variable
    String cardtype = "";
    
    //assign card value to randomly selected value
    switch ( decknumber ) {
      case 1: case 14: case 27: case 40:
        cardtype = "Ace";
        break;
      case 2: case 15: case 28: case 41:
        cardtype = "2";
        break;
      case 3: case 16: case 29: case 42:
        cardtype = "3";
        break;
      case 4: case 17: case 30: case 43:
        cardtype = "4";
        break;   
      case 5: case 18: case 31: case 44:
        cardtype = "5";
        break;   
      case 6: case 19: case 32: case 45:
        cardtype = "6";
        break;  
      case 7: case 20: case 33: case 46:
        cardtype = "7";
        break;
      case 8: case 21: case 34: case 47:
        cardtype = "8";
        break;
      case 9: case 22: case 35: case 48:
        cardtype = "9";
        break;  
      case 10: case 23: case 36: case 49:
        cardtype = "10";
        break;  
      case 11: case 24: case 37: case 50:
        cardtype = "Jack";
        break;  
      case 12: case 25: case 38: case 51:
        cardtype = "Queen";
        break;  
      case 13: case 26: case 39: case 52:
        cardtype = "King";
        break;  
    }
    
    //print randomly selected card
    System.out.println("You picked the " + cardtype + " of " + suit);
    
  }
}